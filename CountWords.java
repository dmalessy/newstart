import java.util.ArrayList;

public class CountWords {
	
	   public static void main(String args[]){
		
		String arrayWords[] = {"Cheesecake","grape","melon","Mooncake","Candy"};
		int count = 0;
		ArrayList<String> arrayWordsList = new ArrayList<>();
		ArrayList<String> rowValues = new ArrayList<>();
		ArrayList<String> rowValues2 = new ArrayList<>();            

		 for(int i = 0 ;  i < arrayWords.length; i++){			
			//Put Result in ArrayList
			arrayWordsList.add(arrayWords[i]);			
			
			//Get all the words start with "m" or "M"
			if (arrayWords[i].toLowerCase().startsWith("m")){				
				rowValues.add(arrayWords[i]);				
				count++;			
					
					//Get all the words start with "m" or "M" and longer than 5 characters
					if (arrayWords[i].length() > 5 ){										
						rowValues2.add(arrayWords[i]);
					}					
			}
      }	  
	  
	  System.out.println("Input: "+arrayWordsList);
	  System.out.println("Result of Business Rule 1: "+count);
	  System.out.println("Result of Business Rule 2: "+rowValues);
	  System.out.println("Result of Business Rule 3: "+rowValues2);        				
      }     
}
